from telegram import Update, ReplyKeyboardMarkup
from telegram.ext import CallbackContext, ConversationHandler, CommandHandler, MessageHandler, Filters

from models import User, Song, Playlist


NAME, SONG = range(2)


end_keyboard = ReplyKeyboardMarkup([
    [
        'Get Playlist',
    ],
    [
        'Create Playlist',
        'Edit Playlist',
    ],
    [
        'HOME',
    ],
])


def check_playlist_exists(update, name):
    chat_id = update.message.from_user.id

    user = User.select().where(User.chat_id == chat_id)
    playlist = Playlist.select().where((Playlist.user == user) & (Playlist.name == name))
    if not len(playlist):
        return True

    return False


def start_remove_song(update: Update, context: CallbackContext):
    context.user_data.clear()
    chat_id = update.message.from_user.id

    try:
        if len(context.args):
            name = context.args[0]

            if check_playlist_exists(update, name):
                context.bot.send_message(chat_id, 'This Playlist dose not exist! Enter again:')
                return ConversationHandler.END

            user = User.select().where(User.chat_id == chat_id)[0]
            playlist = Playlist.select().where((Playlist.user == user) & (Playlist.name == name))
            songs = Song.select().where(Song.playlist == playlist)

            markup = []
            markup.append(['Done'])
            for song in songs:
                markup.append([song.title])
            keyboard = ReplyKeyboardMarkup(markup)

            context.bot.send_message(chat_id,
                                     'Choose the song that you want to from {}'.format(name)
                                     , reply_markup=keyboard)

            context.user_data['name'] = name
            return SONG

    except:
        pass

    user = User.select().where(User.chat_id == chat_id)[0]
    playlists = Playlist.select().where(Playlist.user == user)

    keyboard = ReplyKeyboardMarkup([[playlist.name] for playlist in playlists], resize_keyboard=True)

    context.bot.send_message(chat_id,
                             ('Enter the name of the playlist you wanna remove song!\n\n' +
                              '/cancel to cancel the process'),
                             reply_markup=keyboard)

    return NAME


def get_name(update, context):
    chat_id = update.message.from_user.id
    name = update.message.text

    # check playlist exists
    if check_playlist_exists(update, name):
        context.bot.send_message(chat_id, 'This Playlist dose not exist! Enter again:')
        return

    user = User.select().where(User.chat_id == chat_id)[0]
    playlist = Playlist.select().where((Playlist.user == user) & (Playlist.name == name))[0]
    songs = Song.select().where(Song.playlist == playlist)

    markup = []
    markup.append(['Done'])
    for song in songs:
        markup.append([song.title])
    keyboard = ReplyKeyboardMarkup(markup)

    context.bot.send_message(chat_id,
                             ('Choose the songs that you want to delete from *{}*\n'.format(name) +
                              'next send `Done` to finish!'),
                             parse_mode='Markdown',
                             reply_markup=keyboard)

    context.user_data['name'] = name
    return SONG


def get_song(update: Update, context: CallbackContext):
    chat_id = update.message.from_user.id

    name = context.user_data['name']
    song_title = update.message.text

    user = User.select().where(User.chat_id == chat_id)[0]
    playlist = Playlist.select().where((Playlist.user == user) & (Playlist.name == name))[0]

    Song.delete().where((Song.playlist == playlist) & (Song.title == song_title)).execute()

    songs = Song.select().where(Song.playlist == playlist)

    markup = []
    markup.append(['Done'])
    for song in songs:
        markup.append([song.title])
    keyboard = ReplyKeyboardMarkup(markup)

    context.bot.send_message(chat_id, '*{}* removed from *{}*!'.format(song_title, name),
                             parse_mode='Markdown',
                             reply_markup=keyboard)

    return


def done(update, context: CallbackContext):
    chat_id = update.message.from_user.id

    context.bot.send_message(chat_id, 'Your changes saved successfully!', reply_markup=end_keyboard)

    context.user_data.clear()
    return ConversationHandler.END


def error(update, context):
    chat_id = update.message.chat.id
    context.bot.delete_message(chat_id, update.message.message_id)
    context.bot.send_message(chat_id, ('Oops...\n' +
                                       'Something went wrong! Try again:'))
    return


def cancel(update, context):
    chat_id = update.message.from_user.id
    context.bot.send_message(chat_id, 'Process was canceled!', reply_markup=end_keyboard)

    context.user_data.clear()
    return ConversationHandler.END


HANDLER = ConversationHandler(
    entry_points=[
        CommandHandler('remove', start_remove_song),
        MessageHandler(Filters.text('Remove Song'), start_remove_song),
    ],

    states={

        NAME: [MessageHandler(Filters.text, get_name)],

        SONG: [
            MessageHandler(Filters.text('Done'), done),
            MessageHandler(Filters.text, get_song)
        ],
    },

    fallbacks=[
        MessageHandler(Filters.command, cancel),
        MessageHandler(Filters.text('Cancel'), cancel),
        MessageHandler(Filters.all, error)
    ],

    allow_reentry=True,
)
