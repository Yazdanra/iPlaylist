from telegram import Update, ReplyKeyboardMarkup, ReplyKeyboardRemove
from telegram.ext import CallbackContext, ConversationHandler, CommandHandler, MessageHandler, Filters

from config import ADMIN
from models import User, Song, Playlist


NAME = range(1)


end_keyboard = ReplyKeyboardMarkup([
    [
        'Get Playlist',
    ],
    [
        'Create Playlist',
    ],
    [
        'HOME',
    ],
])


def check_playlist_exists(update, name):
    chat_id = update.message.from_user.id

    user = User.select().where(User.chat_id == chat_id)
    playlist = Playlist.select().where((Playlist.user == user) & (Playlist.name == name))
    if not len(playlist):
        return True

    return False


def start_delete(update: Update, context: CallbackContext):
    chat_id = update.message.from_user.id

    try:
        if len(context.args):
            name = context.args[0]

            if check_playlist_exists(update, name):
                context.bot.send_message(chat_id, 'This Playlist dose not exist! Enter again:')
                return ConversationHandler.END

            context.user_data['name'] = name

            return delete(update, context)

    except:
        pass

    user = User.select().where(User.chat_id == chat_id)[0]
    playlists = Playlist.select().where(Playlist.user == user)

    keyboard = ReplyKeyboardMarkup([[playlist.name] for playlist in playlists], resize_keyboard=True)

    context.bot.send_message(chat_id,
                             ('Enter the name of the playlist you wanna delete!\n\n' +
                              '/cancel to cancel the process'),
                             reply_markup=keyboard)

    return NAME


def get_name(update, context):
    chat_id = update.message.from_user.id
    name = update.message.text

    # check playlist exists
    if check_playlist_exists(update, name):
        context.bot.send_message(chat_id, 'This Playlist dose not exist! Enter again:')
        return

    context.user_data['name'] = name

    return delete(update, context)


def delete(update, context):
    chat_id = update.message.from_user.id
    name = context.user_data['name']

    user = User.select().where(User.chat_id == chat_id)[0]

    playlist = Playlist.select().where((Playlist.user == user) & (Playlist.name == name))[0]

    Song.delete().where(Song.playlist == playlist).execute()

    Playlist.delete().where((Playlist.user == user) & (Playlist.name == name)).execute()

    context.bot.send_message(chat_id, 'Playlist *{}* successfully deleted!'.format(name),
                             parse_mode='Markdown',
                             reply_markup=end_keyboard)

    context.user_data.clear()
    return ConversationHandler.END


def error(update, context):
    chat_id = update.message.chat.id
    context.bot.delete_message(chat_id, update.message.message_id)
    context.bot.send_message(chat_id, ('Oops...\n' +
                                       'Something went wrong! Try again:'))
    return


def cancel(update, context):
    chat_id = update.message.from_user.id
    context.bot.send_message(chat_id, 'Process was canceled!', reply_markup=end_keyboard)

    context.user_data.clear()
    return ConversationHandler.END


HANDLER = ConversationHandler(
    entry_points=[
        CommandHandler('delete', start_delete),
        MessageHandler(Filters.text('Delete Playlist'), start_delete),
    ],

    states={

        NAME: [MessageHandler(Filters.text, get_name)],

    },

    fallbacks=[
        MessageHandler(Filters.command, cancel),
        MessageHandler(Filters.text('Cancel'), cancel),
        MessageHandler(Filters.all, error)
    ],

    allow_reentry=True,
)
